import React, { useState, useRef } from 'react'

// Npm-Packages
import {
  Link
} from 'react-router-dom'
import QrReader from 'react-qr-reader'

// Material-UI-Components-Icons
import { Container, Card, Grid, Button } from '@material-ui/core'
import { ImCancelCircle } from 'react-icons/im'

const ByFile = () => {
  const [scanResultFile, setScanResultFile] = useState('')

  const qrRef = useRef(null)

  // To scan file
  const onScanFile = () => {
    qrRef.current.openImageDialog()
  }

  // For handle errors
  const handleErrorFile = (error) => {
    console.log(error)
  }

  // To get output
  const handleScanFile = (result) => {
    if (result) {
      setScanResultFile(result)
    }
  }

  return (
    <div>
      <Container className="container">
        {/* Main-Card */}
        <Card className="main-text-card responsive-card">
          {/* Cancel-Button */}
          <div className="float-right ml-auto cancel-icon">
            {/* Redirect to main page */}
              <Link to="/"><Button variant="contained" color="secondary"><ImCancelCircle></ImCancelCircle></Button></Link>
          </div>
          <br></br>
          <br></br>
          <Grid xl={12} lg={12} md={12} sm={12} xs={12} className="file-upload-grid">
            {/* Button to generate qr-code-image */}
              <Button className="Generate-button" variant="contained" color="secondary" onClick={onScanFile}>Scan Qr Code</Button>
                {/* Npm library to generate-code */}
                 <QrReader
                    ref={qrRef}
                    delay={300}
                    style={{ width: '30%' }}
                    onError={handleErrorFile}
                    onScan={handleScanFile}
                    legacyMode
                    className="mt-4"
                  />
                {/* Qr code result */}
                  <h3 className="mt-2">Scanned Code: {scanResultFile}</h3>
            </Grid>
          </Card>
        </Container>
    </div>
  )
}

export default ByFile
