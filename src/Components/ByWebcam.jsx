import React, { useState } from 'react'

// Material-UI-Components
import { Container, Card, Grid, Button } from '@material-ui/core'

// NPM-Packages
import { Link } from 'react-router-dom'
import { ImCancelCircle } from 'react-icons/im'
import QrReader from 'react-qr-reader'

const ByWebcam = () => {
  const [scanResultWebCam, setScanResultWebCam] = useState('')

  //  For handle errors
  const handleErrorWebCam = (error) => {
    console.log(error)
  }

  // To get result
  const handleScanWebCam = (result) => {
    if (result) {
      setScanResultWebCam(result)
    }
  }
  return (
    <div>
        <Container className="container">
          <Card className="main-text-card responsive-card">
           {/* Cancel-Button */}
            <div className="float-right ml-auto cancel-icon">
                 <Link to="/"><Button variant="contained" color="secondary"><ImCancelCircle></ImCancelCircle></Button></Link>
             </div>
             <br></br>
           {/* Npm library to generate-code */}
             <Grid xl={12} lg={12} md={12} sm={12} xs={12}>
                <QrReader
                    delay={300}
                    onError={handleErrorWebCam}
                    onScan={handleScanWebCam}
                    className="webcam-reader"
                />
            {/* Result  */}
                <h3 className="webcam-reader mt-2">Scanned By WebCam Code: {scanResultWebCam}</h3>
            </Grid>
          </Card>
        </Container>
    </div>
  )
}

export default ByWebcam
