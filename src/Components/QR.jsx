import React from 'react'

// Material-UI-Components
import { Container, Card, CardContent, Grid, Button } from '@material-ui/core'

// NPM-Packages
import { useHistory } from 'react-router-dom'
import { IoMdQrScanner } from 'react-icons/io'

// Images-set
import first from '../assets/1.gif'
import second from '../assets/2.gif'
import third from '../assets/3.gif'

function QR () {
  const history = useHistory()

  // Redirect to generate QR-Code Image
  const GotogenerateQrCode = async () => {
    history.push('/generate-qr')
  }

  // Redirect to QR-Code Scanner by file
  const GotoUploadbyFile = async () => {
    history.push('/byfile-qr')
  }

  // Redirect to QR-Code Scanner by web
  const GotoUploadbyWebCam = async () => {
    history.push('/bywebcam')
  }

  return (
    <Container className="container">
      {/* Main Card */}
        <Card>
            <h2 className="title-content"><IoMdQrScanner className="scanner-icon" /> Generate Download & Scan QR Code <IoMdQrScanner className="scanner-icon" /></h2>
            <CardContent>
               {/* Main Grid */}
                <Grid container spacing={2}>

                   {/* Grid-1(Genrearte QR Code) */}
                   <Grid xl={4} lg={4} md={6} sm={12} xs={12} className="ml-2 main-divs">
                        <img src={first} className="main-images rounded-circle"></img>
                            <br/>
                            <br/>
                        <Button className="Generate-button" variant="contained"
                            color="secondary"
                            onClick={() => GotogenerateQrCode()}
                            >Generate</Button>
                    </Grid>

                    {/* Grid-2(QR Code Scanner by file) */}
                    <Grid item xl={4} lg={4} md={6} sm={12} xs={12} className="main-divs">
                         <img src={second} className="main-images rounded-circle"></img>
                            <br/>
                            <br/>
                        <Button className="Generate-button" variant="contained" color="secondary"
                          onClick={() => GotoUploadbyFile()}
                        >QR code Scanner by File</Button>
                      </Grid>

                    {/* Grid-3(QR Code Scanner by webcam) */}
                      <Grid item xl={4} lg={4} md={6} sm={12} xs={12} className=" main-divs">
                         <img src={third} className="main-images rounded-circle "></img>
                            <br/>
                            <br/>
                        <Button className="Generate-button " variant="contained" color="secondary"
                        onClick={() => GotoUploadbyWebCam()}
                        >QR code Scanner by WebCam</Button>
                      </Grid>
                  </Grid>
              </CardContent>
          </Card>
    </Container>
  )
}

export default QR
