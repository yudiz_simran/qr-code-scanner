import React, { Component } from 'react'
import QrReader from 'react-qr-reader'

import '../App.css'
 
class Main extends Component {
  state = {
    result: 'No result'
  }
 
  handleScan = data => {
    if (data) {
      this.setState({
        result: data
      })
    }
  }
  handleError = err => {
    console.error(err)
  }
  render() {
    return (
      <>
      <div className="col-lg-12 row"> 
        <div classname="col-lg-6">
                <QrReader
                delay={300}
                onError={this.handleError}
                onScan={this.handleScan}
                className="main-content "
                />
            </div>
            <div className="result col-lg-6 ">
                 <p>{this.state.result}</p>
            </div>
      </div>
      </>
    )
  }
}

export default Main