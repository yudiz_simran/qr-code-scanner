import React, { useState } from 'react'

// Material-UI-Packages
import { Container, Card, Grid, TextField, Button } from '@material-ui/core'

// NPM-Packages
import { ImCancelCircle } from 'react-icons/im'
import QRCode from 'qrcode'
import {
  Link
} from 'react-router-dom'

const GenerateQR = () => {
  const [text, setText] = useState('')
  const [imageUrl, setImageUrl] = useState('')

  // To generate QR Code
  const generateQrCode = async () => {
    try {
      const response = await QRCode.toDataURL(text)
      setImageUrl(response)
    } catch (error) {
      console.log(error)
    }
  }

  return (
        <>
         <Container className="container">
           <Card className="main-text-card responsive-card">

             {/* Redirect to Main-Component */}
             <div className="float-right ml-auto cancel-icon">
                 <Link to="/"><Button variant="contained" color="secondary"><ImCancelCircle></ImCancelCircle></Button></Link>
             </div>
             <br></br>
              <Grid xl={12} lg={12} md={12} sm={12} xs={12}>
                {/* Input Field to generate QR-Code */}
                  <TextField label="Enter Text Here" className="col-lg-12" onChange={(e) => setText(e.target.value)}/>
                      <Button className="g-button mt-3" variant="contained"
                          color="primary" onClick={() => generateQrCode()}>Generate</Button>
                      <br />
                      <br />
                  <div className="get-image">
                    {
                      imageUrl
                        ? <a href={imageUrl} download> <img src={imageUrl} alt="img"/></a>
                        : null}
                   </div>
                    </Grid>
          </Card>
          </Container>
        </>
  )
}

export default GenerateQR
