import React from 'react'
import './App.css'

// NPM-Packages
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import { ToastContainer, Flip } from 'react-toastify'

// Bootstrap-Library
import 'bootstrap/dist/css/bootstrap.min.css'

// React-Toastify-Library
import 'react-toastify/dist/ReactToastify.css'

// Components
import QR from './Components/QR'
import GenerateQR from './Components/GenerateQR'
import ByFile from './Components/ByFile'
import ByWebcam from './Components/ByWebcam'

function App () {
  return (
    <div className="App">
       <Router>
        <Switch>

           {/* Route to generate QR Code by web cam */}
              <Route path="/bywebcam">
                <ByWebcam />
              </Route>

           {/* Route to generate QR Code by file */}
              <Route path="/byfile-qr">
                        <ByFile />
              </Route>

            {/* Route to generate QR Code  */}
              <Route path="/generate-qr">
                <GenerateQR />
              </Route>

            {/* Main-Component */}
              <Route path="/">
                <QR />
              </Route>
         {/* Toastify Notifications */}
          <ToastContainer
                position="top-right"
                transition={Flip}
                autoClose={3000}
                hideProgressBar={true}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                // pauseOnFocusLoss
                draggable
                pauseOnHover
          />
        </Switch>
      </Router>
    </div>
  )
}

export default App
